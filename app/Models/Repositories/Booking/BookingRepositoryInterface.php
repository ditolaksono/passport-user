<?php


namespace App\Models\Repositories\Booking;


interface BookingRepositoryInterface
{
    public function getBookingByCheckInAndOutWithActiveStatus($checkIn, $checkOut);

    public function getLastBookingId();

    public function storeBooking(array $params);

    public function getBookingById($bookingId);

    public function getAllBooking($custId);

    public function getBookingByCode($bookingCode);

    public function updateVANumber($vaNumber);

    public function updateBookingPaymentStatus(array $params, $bookingId);

    public function getBookingRoomByRangeDate($roomType,$checkIn,$checkOut, $toSql=true);
}
