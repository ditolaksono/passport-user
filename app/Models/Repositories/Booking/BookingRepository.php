<?php


namespace App\Models\Repositories\Booking;


use App\Models\Booking;

class BookingRepository implements BookingRepositoryInterface
{
    private $model;

    public function __construct(Booking $model)
    {
        $this->model = $model;
    }

    public function getBookingByCheckInAndOutWithActiveStatus($checkIn, $checkOut)
    {
        return $this->model->whereBetween('book_check_in', [
            $checkIn, $checkOut
        ])->whereIn('book_tenant_status', [1,2,3,7])->get();
    }

    public function getLastBookingId()
    {
        return $this->model->max('book_id');
    }

    public function updateCancelUnpaidBookDeposit($custId)
    {
        $this->model
            ->where('book_cust_id', $custId)
            ->where('book_tenant_status', 1)
            ->where('book_payment_status_id', 1)
            ->update([
                'book_tenant_status' => 8,
                'book_payment_status_id' => 5,
                'book_update_by' => 'WEBSITE-BOOKING'
            ]);

        return $this->model;
    }

    public function storeBooking(array $params)
    {
        $this->model->insert($params);

        $this->model = $this->model->where('book_no', $params['book_no'])->first();
        return $this->model;
    }

    public function getAllBooking($custId)
    {
        return $this->model
            ->where('book_cust_id', $custId)
            ->get();
    }

    public function getBookingById($bookingId)
    {
        return $this->model
            ->where('book_id', $bookingId)
            ->first();
    }

    public function getBookingByCode($bookingCode)
    {
        return $this->model
            ->select('book_id','book_cust_id','book_building_id','book_room_type_id','book_room_id',
                'book_check_in','book_check_out','book_bookby_id','book_status_id', 'book_promo_id',
                'book_total_price','book_va_no', 'book_va_status', 'book_payment_type_id','book_payment_status_id',
                'book_reschedule_status', 'book_pay_log','book_extended_status','book_del_status')
            ->where('book_no', $bookingCode)
            ->first();
    }

    public function updateVANumber($vaNumber)
    {
        $this->model
            ->where('book_va_no', $vaNumber)
            ->update([
                'book_va_no' => 'EXPIRED',
                'book_update_by' => 'PAYMENT-VALIDATION'
            ]);

        return $this->model;
    }

    public function updateBookingPaymentStatus(array $params, $bookingId)
    {
        $this->model
            ->where('book_id', $bookingId)
            ->update($params);

        $this->model = $this->model->where('book_id', $bookingId)->first();

        return $this->model;
    }

    public function getBookingRoomByRangeDate($roomType,$checkIn,$checkOut, $toSql=true)
    {
        $query = $this->model->select('book_room_id')
            ->where('book_room_type_id','=',(int)$roomType)
            ->where(function($in){
                $in->whereIn('book_status_id', [1,6,7,11]);
                $in->orWhere(function($inx){
                    $inx->where('book_va_status','=',0);
                    $inx->where('book_status_id','=',3);
                });
            })
            ->where('book_del_status','=','0')
            ->where(function($qr) use ($checkIn,$checkOut){
                $qr->where(function($qrdeep) use ($checkIn, $checkOut){
                    $qrdeep->where(function($qrdeepxx1) use ($checkIn){
                        $qrdeepxx1->whereRaw("? BETWEEN book_check_in AND book_check_out",[$checkIn]);
                    });
                    $qrdeep->orWhere(function($qrdeepxx2) use ($checkOut){
                        $qrdeepxx2->whereRaw("? BETWEEN book_check_in AND book_check_out",[$checkOut]);
                    });
                });
                $qr->orWhere(function($qrdeepx) use($checkIn, $checkOut){
                    $qrdeepx->orWhere(function($qrdeepxx3) use ($checkIn, $checkOut) {
                        $qrdeepxx3->orWhereBetween('book_check_in', [$checkIn, $checkOut]);
                    });
                    $qrdeepx->orWhere(function($qrdeepxx4) use ($checkIn, $checkOut) {
                        $qrdeepxx4->orWhereBetween('book_check_out', [$checkIn, $checkOut]);
                    });
                });
            })
            ->distinct();
        return ($toSql)?$query:$query->get();
    }

}
