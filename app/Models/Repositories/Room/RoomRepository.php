<?php


namespace App\Models\Repositories\Room;


use App\Models\Repositories\Booking\BookingRepositoryInterface;
use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Support\Facades\DB;

class RoomRepository implements RoomRepositoryInterface
{
    private $model;
    private $bookingRepository;

    public function __construct
    (
        Room $model,
        BookingRepositoryInterface $bookingRepository
    )
    {
        $this->model = $model;
        $this->bookingRepository = $bookingRepository;
    }

    public function getRoomAvailable(RoomType $roomType, $checkIn, $checkOut)
    {
        $bookingList = $this->bookingRepository->getBookingByCheckInAndOutWithActiveStatus($checkIn, $checkOut)
            ->pluck('book_room_id');

        return $this->model
                ->where('rooms_type_id', $roomType->roomtype_id)
                ->where('rooms_status_id', 1)
                ->whereNotIn('rooms_id', $bookingList);
    }

    public function getRoomAvailabilityCount(RoomType $roomType, $checkIn, $checkOut)
    {
        return $this->getRoomAvailable($roomType, $checkIn, $checkOut)->count();
    }

    public function getOneRoomAvailable(RoomType $roomType, $checkIn, $checkOut)
    {
        return $this->getRoomAvailable($roomType, $checkIn, $checkOut)->first();
    }

    /*
    * @param : integer roomType
    *          array date
    *              sd : start date (string)
    *              ed : all available end date array(string)
    *                  date[ed][0] = '2020-02-01'
    * @return : array
    */
    public  function getAvailableRoomMultipleEndDate($roomType,array $date){
        $binding = []; $raw = '';
        for($i=0;$i<count($date['ed']);$i++){
            if($i != 0){
                $raw .= '  UNION ALL  ';
            }
            $result = $this->bookingRepository->getBookingRoomByRangeDate($roomType,$date['sd'],$date['ed'][$i]);
            $sql = $result->toSql();
            $raw .= "SELECT COUNT(rooms_id) as cc FROM rooms WHERE rooms_type_id = ? AND rooms_status_id = ? AND rooms_id NOT in($sql)";
            $binding[] = $roomType; $binding[] = 1;
            foreach($result->getBindings() as $k=>$v){
                $binding[] = $v;
            }
        }
        return  DB::select($raw,$binding);
    }

    public  function getAvailableRoomByDate($roomType,$sd,$ed){
        $result = $this->booking->getBookingRoomByRangeDate($roomType,$sd,$ed); $sql = $result->toSql(); $binding=[];
        $raw =  "SELECT COUNT(rooms_id) as cc FROM rooms WHERE rooms_type_id = ? AND rooms_status_id = ? AND rooms_id NOT in($sql)";
        $binding[] = $roomType; $binding[] = 1;
        foreach($result->getBindings() as $k=>$v){
            $binding[] = $v;
        }
        return  DB::select($raw,$binding);
    }

}
