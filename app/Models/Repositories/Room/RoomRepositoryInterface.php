<?php


namespace App\Models\Repositories\Room;


use App\Models\RoomType;

interface RoomRepositoryInterface
{
    public function getRoomAvailable(RoomType $roomType, $checkIn, $checkOut);

    public function getRoomAvailabilityCount(RoomType $roomType, $checkIn, $checkOut);

    public function getOneRoomAvailable(RoomType $roomType, $checkIn, $checkOut);

    public function getAvailableRoomMultipleEndDate($roomType, array $date);

    public  function getAvailableRoomByDate($roomType,$sd,$ed);
}
