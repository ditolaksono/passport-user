<?php


namespace App\Models\Repositories\Customer;

use Illuminate\Support\Collection;

interface CustomerRepositoryInterface
{
    public function getProfileById($id);

    public function getDataById($id);

    public function getDataByEmail($email);

    public function getDataByEmailOrPhone($email, $phone);

    public function storeData(array $data);

    public function updateDataById($id, array $data);
}
