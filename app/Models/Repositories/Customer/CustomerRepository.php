<?php


namespace App\Models\Repositories\Customer;

use App\Models\Customer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CustomerRepository implements CustomerRepositoryInterface
{
    private $model;

    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    public function getProfileById($id)
    {
        return $this->model->where('cust_id', $id)
                    ->where('cust_del_status','0')
                    ->select('cust_name','cust_gender','cust_image','cust_address','cust_dob','cust_place_of_birth','cust_phone','cust_email','cust_member_type_id')->first();
    }

    public function getDataById($id)
    {
        return $this->model->where('cust_id', $id)
                    ->where('cust_del_status','0')->first();
    }

    public function getDataByEmail($email)
    {
        return $this->model->where('cust_email', $email)
                    ->where('cust_del_status','0')->first();
    }

    public function getDataByEmailOrPhone($email,$phone)
    {
        return $this->model->where('cust_email', $email)
                    ->orWhere('cust_phone',$phone)
                    ->where('cust_del_status','0')->first();
    }

    public function storeData($data)
    {
        try{
            DB::beginTransaction();

            $newData = new Customer;

            $newData->cust_hexid = $data['hexid'];
            $newData->cust_name = $data['name'];
            $newData->cust_email = $data['email'];
            $newData->cust_phone = $data['phone'];
            $newData->cust_hashkey = $data['hashkey'];
            $newData->cust_password = $data['password'];
            $newData->cust_reg_by = $data['reg_by'];
            $newData->cust_gender = $data['gender'];
            $newData->cust_dob = $data['dob'];
            $newData->cust_interest = $data['interest'];
            $newData->cust_is_active = $data['is_active'];
            $newData->cust_create_date = $data['create_date'];

            $newData->save();

            DB::commit();

            return $newData;
        } catch (\Exception $e){
            DB::rollback();

            return false;
        }
    }

    public function updateDataById($id,$data)
    {
        try{
            DB::beginTransaction();

            $this->model->where('cust_id', $id)->update($data);

            DB::commit();

            return true;
        } catch (\Exception $e){
            DB::rollback();

            return false;
        }
    }
}
