<?php


namespace App\Models\Repositories\Promo;

use App\Models\Promo;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PromoRepository implements PromoRepositoryInterface
{
    private $model;

    public function __construct(Promo $model)
    {
        $this->model = $model;
    }

    public function getActiveDataByAffiliate($affiliate)
    {
        return $this->model->where('promo_validation', $affiliate)
                            ->whereDate('promo_start_date', '<=', Carbon::now())
                            ->whereDate('promo_end_date', '>=', Carbon::now())
                            ->where('promo_del_status','0')
                            ->first();
    }

    public function getPromoCode($promoCode)
    {
        return $this->model->where('promo_id', $promoCode)
            ->where('promo_active','=','0')
            ->select('promo_id','promo_name','promo_percent','promo_percent_max_value','promo_value',
            'promo_start_date','promo_end_date','promo_start_book','promo_end_book','promo_minroomprice',
                'promo_maxroomprice','promo_qty','promo_used')
            ->first();
    }
}
