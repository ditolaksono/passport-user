<?php


namespace App\Models\Repositories\Promo;

use Illuminate\Support\Collection;

interface PromoRepositoryInterface
{
    public function getActiveDataByAffiliate($affiliate);
    public function getPromoCode($promoCode);
}
