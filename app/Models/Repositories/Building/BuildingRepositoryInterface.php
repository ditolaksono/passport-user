<?php


namespace App\Models\Repositories\Building;

use App\Models\Building;
use Illuminate\Support\Collection;

interface BuildingRepositoryInterface
{
    public function search(array $params = null);

    public function getBuildingByName($buildName);

    public function getByBuildProvinsi(string $param);

    public function getByBuildKabupaten(string $param);

    public function getByBuildKecamatan(string $param);

    public function getByBuildId($buildId);

    public function getByBuildName(string $param);

    public function getRecommendedBuilding(): Collection;

    public function getBuildingByNameAndExactLocation($buildName, $params = []);

    public function getBuildingByDynamicQuery($params = []);

    public function getBuildingByDistance($lat, $lng);

    public function getByMinPrice($price);

    public function getByMaxPrice($price);

    public function getByCategory($category);

    public function getBuildingThatNotDeletedAndHasRoomTypeWithPrice();
}
