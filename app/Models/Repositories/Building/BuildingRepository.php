<?php


namespace App\Models\Repositories\Building;

use App\Models\Building;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class BuildingRepository implements BuildingRepositoryInterface
{
    private $model;

    public function __construct(Building $model)
    {
        $this->model = $model;
    }

    public function search(array $params = null)
    {
        if (count($params) > 0) $this->model = $this->getByBuildProvinsi($params[0]);

        if (count($params) > 1) $this->model = $this->getByBuildKabupaten($params[1]);

        if (count($params) > 2) $this->model = $this->getByBuildKecamatan($params[2]);

        if (count($params) > 3) $this->model = $this->getByBuildName($params[3]);

        return $this->model->get();
    }

    public function getByBuildProvinsi(string $param)
    {
        $this->model = $this->model
            ->where('build_provinsi', 'like', "%$param%");

        return $this->model;
    }

    public function getByBuildKabupaten(string $param)
    {
        $this->model = $this->model
            ->where('build_kabupaten', 'like', "%$param%");

        return $this->model;
    }

    public function getByBuildKecamatan(string $param)
    {
        $this->model = $this->model
            ->where('build_kecamatan', 'like', "%$param%");

        return $this->model;
    }

    public function getByBuildId($buildId)
    {
        $this->model = $this->model
            ->where('build_id', $buildId)
            ->where('build_del_status', '0')
            ->first();

        return $this->model;
    }

    public function getByBuildName(string $param)
    {
        $this->model = $this->model
            ->where('build_name', 'like', "%$param%");

        return $this->model;
    }

    public function getBuildingByName($buildName)
    {
        return $this->model->where('build_name', 'like', "%$buildName%")->get();
    }

    public function getRecommendedBuilding(): Collection
    {
        return $this->model->where([
            'build_recommendation' => 1,
            'build_del_status'     => '0',
        ])->get();
    }

    public function getBuildingByNameAndExactLocation($buildName, $params = [])
    {
        $this->model = $this->model->where('build_name', 'like', "%$buildName%");

        foreach ($params as $key => $param) {
            $this->model = $this->model->where($key, 'like', "%$param%");
        }

        return $this->model->get();
    }

    public function getBuildingByDynamicQuery($params = [])
    {
        foreach ($params as $key => $param) {
            $this->model = $this->model->where($key, 'like', "%$param%");
        }

        return $this->model;
    }

    public function getBuildingByDistance($lat, $lng, $radius = 3)
    {
        $this->model = $this->model = $this->model
            ->havingRaw(
                "(6371 * acos(cos(radians($lng)) * cos(radians(build_longitude)) * cos(radians(build_latitude)
                    - radians($lat)) + sin(radians($lng)) * sin(radians(build_longitude)))) < ?",
                [
                    $radius,
                ]
            );

        return $this->model;
    }

    public function getByMinPrice($price)
    {
        return $this->model->whereHas('roomTypes', function (Builder $val) use ($price) {
            $val->where('roomtype_price', '>=', $price);
        });
    }

    public function getByMaxPrice($price)
    {
        return $this->model->whereHas('roomTypes', function (Builder $val) use ($price) {
            $val->where('roomtype_price', '<=', $price);
        });
    }

    public function getByCategory($category)
    {
        return $this->model->where('build_service', $category);
    }

    public function getBuildingThatNotDeletedAndHasRoomTypeWithPrice()
    {
        $this->model = $this->model->whereHas('roomTypes', function (Builder $val) {
            $val->where('roomtype_del_status', '=', '0')
                ->where('roomtype_price', '<>', null);
        });

        return $this->model;
    }
}
