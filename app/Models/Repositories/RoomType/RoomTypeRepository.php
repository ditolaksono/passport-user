<?php


namespace App\Models\Repositories\RoomType;


use App\Models\Repositories\Booking\BookingRepositoryInterface;
use App\Models\RoomType;

class RoomTypeRepository implements RoomTypeRepositoryInterface
{
    private $model;
    private $bookingRepository;

    public function __construct(
        RoomType $roomType,
        BookingRepositoryInterface $bookingRepository
    )
    {
        $this->model = $roomType;
        $this->bookingRepository = $bookingRepository;
    }

    public function getRoomTypeById($id)
    {
        $this->model = $this->model
            ->select('roomtype_build_id', 'roomtype_name', 'roomtype_bed', 'roomtype_photos',
            'roomtype_price', 'roomtype_description')
            ->where('roomtype_id', $id);

        return $this->model->first();
    }

    public function getRoomTypeAndBuildingById($id){
        $this->model = $this->model
            ->select('roomtype_build_id', 'roomtype_name', 'roomtype_bed', 'roomtype_photos',
                'roomtype_price', 'roomtype_description','build_name','build_kabupaten',
                'build_photos')
            ->join('building','build_id','=','roomtype_build_id')
            ->where('roomtype_id', $id)
            ->where('roomtype_del_status','=','0');
        return $this->model->first();
    }
}
