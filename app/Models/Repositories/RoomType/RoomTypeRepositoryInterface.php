<?php


namespace App\Models\Repositories\RoomType;


use App\Models\RoomType;

interface RoomTypeRepositoryInterface
{
    public function getRoomTypeById($id);
    public function getRoomTypeAndBuildingById($id);
}
