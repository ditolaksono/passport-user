<?php


namespace App\Models\Repositories;


use App\Models\Repositories\Booking\BookingRepository;
use App\Models\Repositories\Booking\BookingRepositoryInterface;
use App\Models\Repositories\Building\BuildingRepository;
use App\Models\Repositories\Building\BuildingRepositoryInterface;
use App\Models\Repositories\Customer\CustomerRepository;
use App\Models\Repositories\Customer\CustomerRepositoryInterface;
use App\Models\Repositories\Facilities\FacilitiesRepository;
use App\Models\Repositories\Facilities\FacilitiesRepositoryInterface;
use App\Models\Repositories\Promo\PromoRepository;
use App\Models\Repositories\Promo\PromoRepositoryInterface;
use App\Models\Repositories\TncPromo\TncPromoRepository;
use App\Models\Repositories\TncPromo\TncPromoRepositoryInterface;
use App\Models\Repositories\Register\RegisterRepository;
use App\Models\Repositories\Register\RegisterRepositoryInterface;
use App\Models\Repositories\Room\RoomRepository;
use App\Models\Repositories\Room\RoomRepositoryInterface;
use App\Models\Repositories\RoomType\RoomTypeRepository;
use App\Models\Repositories\RoomType\RoomTypeRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class CommonRepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            BuildingRepositoryInterface::class,
            BuildingRepository::class
        );

        $this->app->bind(
            BookingRepositoryInterface::class,
            BookingRepository::class
        );

        $this->app->bind(
            RoomTypeRepositoryInterface::class,
            RoomTypeRepository::class
        );

        $this->app->bind(
            RoomRepositoryInterface::class,
            RoomRepository::class
        );

        $this->app->bind(
            FacilitiesRepositoryInterface::class,
            FacilitiesRepository::class
        );

        $this->app->bind(
            CustomerRepositoryInterface::class,
            CustomerRepository::class
        );

        $this->app->bind(
            RegisterRepositoryInterface::class,
            RegisterRepository::class
        );

        $this->app->bind(
            PromoRepositoryInterface::class,
            PromoRepository::class
        );

        $this->app->bind(
            TncPromoRepositoryInterface::class,
            TncPromoRepository::class
        );
    }
}
