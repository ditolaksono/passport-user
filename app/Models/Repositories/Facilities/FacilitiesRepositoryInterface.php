<?php


namespace App\Models\Repositories\Facilities;


use Illuminate\Support\Collection;

interface FacilitiesRepositoryInterface
{
    public function getAllBuildingFacilities();

    public function getAllRoomTypeFacilities();

    public function getDistinctedBuildingFacilities(): Collection;

    public function getDistinctedRoomTypeFacilities();
}
