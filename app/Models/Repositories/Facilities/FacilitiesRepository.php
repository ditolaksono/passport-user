<?php


namespace App\Models\Repositories\Facilities;


use App\Models\Facilities;
use Illuminate\Support\Collection;

class FacilitiesRepository implements FacilitiesRepositoryInterface
{
    private $model;

    public function __construct(Facilities $model)
    {
        $this->model = $model;
    }

    public function getAllBuildingFacilities()
    {
        return $this->model->where('facilities_by', 'like', '%Building%')->get();
    }

    public function getAllRoomTypeFacilities()
    {
        return $this->model->where('facilities_by', 'like', '%Room Type%')->get();
    }

    public function getDistinctedBuildingFacilities(): Collection
    {
        return $this->model->where('facilities_by', 'like', '%Building%')
            ->distinct()->orderBy('facilities_name', 'ASC')
            ->get(['facilities_name', 'facilities_image']);
    }

    public function getDistinctedRoomTypeFacilities()
    {
        // TODO: Implement getDistinctedRoomTypeFacilities() method.
    }
}
