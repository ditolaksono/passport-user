<?php


namespace App\Models\Repositories\Register;

use App\Models\Register;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RegisterRepository implements RegisterRepositoryInterface
{
    private $model;

    public function __construct(Register $model)
    {
        $this->model = $model;
    }

    public function getDataByEmail($email)
    {
        return $this->model->select('reg_id as id',
                                    'reg_email as email',
                                    'reg_name as name',
                                    'reg_phone as phone',
                                    'reg_password as password',
                                    'reg_token as token',
                                    'reg_gender as gender',
                                    'reg_dob as dob',
                                    'reg_interest as interest')
                            ->where('reg_email', $email)
                            ->where('reg_del_status','0')->first();
    }

    public function getDataByEmailOrPhone($email,$phone)
    {
        return $this->model->where('reg_email', $email)->orWhere('reg_phone',$phone)->where('reg_del_status','0')->first();
    }

    public function getDataByToken($token)
    {
        return $this->model->select('reg_id as id',
                                    'reg_email as email',
                                    'reg_name as name',
                                    'reg_phone as phone',
                                    'reg_password as password',
                                    'reg_type as type',
                                    'reg_token as token',
                                    'reg_gender as gender',
                                    'reg_dob as dob',
                                    'reg_interest as interest')
                            ->where('reg_token', $token)
                            ->where('reg_del_status','0')->first();
    }

    public function storeData($data)
    {
        try{
            DB::beginTransaction();

            $newData = new Register;

            $newData->reg_name = $data['name'];
            $newData->reg_email = $data['email'];
            $newData->reg_phone = $data['phone'];
            $newData->reg_password = $data['password'];
            $newData->reg_type = $data['type'];
            $newData->reg_token = $data['token'];
            $newData->reg_gender = $data['gender'];
            $newData->reg_dob = $data['dob'];
            $newData->reg_interest = $data['interest'];
            $newData->reg_del_status = $data['del_status'];

            $newData->save();

            DB::commit();

            $newUser = $this->getDataByEmail($newData->reg_email);

            return $newUser;
        } catch (\Exception $e){
            DB::rollback();

            return false;
        }
    }

    public function deleteData($id)
    {
        try{
            DB::beginTransaction();

            $this->model->where('reg_id', $id)->delete();

            DB::commit();

            return true;
        } catch (\Exception $e){
            DB::rollback();

            return false;
        }
    }

    public function softDeleteData($id)
    {
        try{
            DB::beginTransaction();

            $this->model->where('reg_id', $id)->update(['reg_del_status' => '1']);

            DB::commit();

            return true;
        } catch (\Exception $e){
            DB::rollback();

            return false;
        }
    }
}
