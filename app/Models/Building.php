<?php

namespace App\Models;

use App\Http\Services\BuildingServices;
use App\Models\RoomType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * An Eloquent Model: 'Building'
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $build_id
 * @property string|null $build_name
 * @property string|null $build_total_room
 * @property string|null $build_start_contract
 * @property string|null $build_end_contract
 * @property string|null $build_no_contract
 * @property string|null $build_descriptions
 * @property int|null $build_address_rt
 * @property int|null $build_address_rw
 * @property string|null $build_address
 * @property string|null $build_kelurahan
 * @property string|null $build_kecamatan
 * @property string|null $build_kabupaten
 * @property string|null $build_provinsi
 * @property int|null $build_address_postal
 * @property string|null $build_longitude
 * @property string|null $build_latitude
 * @property string|null $build_google_id
 * @property mixed|null $build_nearby
 * @property string|null $build_prod_name
 * @property string|null $build_sub_prod
 * @property string|null $build_service
 * @property int|null $build_category_id
 * @property int|null $build_contract_id
 * @property int|null $build_grace_period_id
 * @property int|null $build_status_building
 * @property int|null $build_audit
 * @property mixed|null $build_photos
 * @property string|null $build_create_by
 * @property string|null $build_create_date
 * @property string|null $build_update_by
 * @property string|null $build_update_date
 * @property string $build_del_status
 * @property float|null $build_rate_value
 * @property int|null $build_rate_count
 * @property int|null $build_recomendation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildAddressPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildAddressRt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildAddressRw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildAudit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildContractId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildCreateBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildCreateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildDelStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildDescriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildEndContract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildGracePeriodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildKabupaten($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildKecamatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildKelurahan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildNearby($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildNoContract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildPhotos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildProdName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildProvinsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildRateCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildRateValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildRecomendation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildStartContract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildStatusBuilding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildSubProd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildTotalRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildUpdateBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Building whereBuildUpdateDate($value)
 * @mixin \Eloquent
 */
class Building extends Model
{
    protected $primaryKey = 'build_id';

    protected $table = "building";

    protected $hidden = [
        'build_del_status',
        'build_latitude',
        'build_longitude',
        'build_start_contract',
        'build_end_contract',
        'build_no_contract',
        'build_address_rt',
        'build_address_rw',
        'build_total_room',
        'build_address_postal',
        'build_google_id',
        'build_prod_name',
        'build_sub_prod',
        'build_category_id',
        'build_contract_id',
        'build_grace_period_id',
        'build_status_building',
        'build_audit',
        'build_create_by',
        'build_create_date',
        'build_update_by',
        'build_update_date',
        'build_rate_value',
        'build_rate_count',
        'build_recomendation'
    ];

    public function roomTypes()
    {
        return $this->hasMany(RoomType::class, 'roomtype_build_id', 'build_id');
    }

    public function facilities()
    {
        return $this->hasMany(Facilities::class, 'facilities_ref_id', 'build_id');
    }

    public function getBuildingDetailBySlug(array $params)
    {
        return $this
            ->where('build_provinsi', 'like', "%$params[0]%")
            ->where([
                'build_kabupaten' => $params[1],
                'build_kecamatan' => $params[2],
                'build_name'      => $params[3]
            ])->firstOrFail();
    }
}
