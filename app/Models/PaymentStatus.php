<?php

namespace App\Models;

use App\Models\Building;
use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    protected $primaryKey = 'payment_stat_id';

    protected $table = 'payment_status_bc';

    public function booking()
    {
        return $this->hasMany(Booking::class, 'payment_stat_id', 'book_payment_status_id');
    }
}
