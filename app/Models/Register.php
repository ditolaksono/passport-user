<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Register extends Model
{
    protected $table = "register";
    protected $primaryKey = 'reg_id';
    const CREATED_AT = 'reg_create_date';
    const UPDATED_AT = 'reg_update_date';

    protected $fillable = [
        'reg_name',
        'reg_email',
        'reg_phone',
        'reg_password',
        'reg_token',
        'reg_type',
        'reg_gender',
        'reg_dob',
        'reg_interest',
        'reg_create_by',
        'reg_create_date',
        'reg_update_by',
        'reg_update_date',
        'reg_del_status'
    ];
}
