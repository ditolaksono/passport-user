<?php

namespace App\Models;

use App\Models\Building;
use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $primaryKey = 'roomtype_id';

    protected $table = 'room_type';

    protected $hidden = [
        'roomtype_kosong',
        'roomtype_km',
        'roomtype_km_total',
        'roomtype_isi',
        'roomtype_size',
        'roomtype_com_price',
        'roomtype_create_by',
        'roomtype_create_date',
        'roomtype_update_by',
        'roomtype_update_date',
        'roomtype_del_status',
        'roomtype_rate_value',
        'roomtype_rate_count'
    ];

    public function building()
    {
        return $this->belongsTo(Building::class, 'roomtype_build_id', 'build_id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'rooms_type_id', 'book_id');
    }

    public function rooms()
    {
        return $this->hasMany(Room::class, 'rooms_type_id', 'id');
    }
}
