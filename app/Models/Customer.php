<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;

class Customer extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens;

    protected $table = "customer";

    protected $primaryKey = 'cust_id';

    const CREATED_AT = 'cust_create_date';

    const UPDATED_AT = 'cust_update_date';

    protected $fillable = [
        'cust_hexid',
        'cust_name',
        'cust_gender',
        'cust_image',
        'cust_photo_idcard',
        'cust_nik',
        'cust_address',
        'cust_dob',
        'cust_place_of_birth',
        'cust_phone',
        'cust_email',
        'cust_password',
        'cust_hashkey',
        'cust_reg_by',
        'cust_gender',
        'cust_dob',
        'cust_interest',
        'cust_login_multidevice',
        'cust_login_hashkey',
        'cust_login_hashpass',
        'cust_facebook_id',
        'cust_google_id',
        'cust_facebook_url',
        'cust_google_url',
        'cust_device',
        'cust_member_type_id',
        'cust_id_refferal',
        'cust_is_active',
        'cust_create_by',
        'cust_create_date',
        'cust_update_by',
        'cust_update_date',
        'cust_del_status'
    ];

//    public function getJWTIdentifier()
//    {
//        return $this->getKey();
//    }
//
//    /**
//     * Return a key value array, containing any custom claims to be added to the JWT.
//     *
//     * @return array
//     */
//    public function getJWTCustomClaims()
//    {
//        return ['iss' => env('JWT_ISS'),
//                'aud' => env('JWT_AUD')];
//    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'cust_id', 'book_cust_id');
    }

    public function findForPassport($username)
    {
        return $this->where('cust_email', $username)->first();
    }

    public function validateForPassportPasswordGrant($password)
    {
        return sha1($password) === $this->cust_password;
    }
}
