<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $primaryKey = 'book_id';

    protected $table = 'booking';

    const CREATED_AT = 'book_create_date';

    const UPDATED_AT = 'book_update_date';

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'book_cust_id', 'cust_id');
    }

    public function building()
    {
        return $this->belongsTo(Building::class, 'book_building_id', 'build_id');
    }

    public function roomType()
    {
        return $this->belongsTo(RoomType::class, 'book_room_type_id', 'roomtype_id');
    }

    public function tenantStatus()
    {
        return $this->belongsTo(TenantStatus::class, 'book_tenant_status', 'tenant_stat_id');
    }

    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class, 'book_payment_status_id', 'payment_stat_id');
    }
}
