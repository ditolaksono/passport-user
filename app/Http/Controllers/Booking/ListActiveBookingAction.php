<?php

namespace App\Http\Controllers\Booking;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ListActiveBookingAction extends Controller
{
    public function __invoke(Request $request)
    {
        $data = Booking::where('user_id', Auth::user()->id)->get();

        return response()->json($data);
    }
}
