<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GetUserProfileAction extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(Auth::user());
    }
}
