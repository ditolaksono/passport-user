<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Laravel\Passport\Exceptions\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class AccessTokenController
 * @package App\Http\Controllers\Auth
 *
 * \Laravel\Passport\Http\Controllers\AccessTokenController
 *
 */
class AccessTokenController extends \Laravel\Passport\Http\Controllers\AccessTokenController
{
    public function issueToken(ServerRequestInterface $request)
    {
        try {
            $username = $request->getParsedBody()['username'];

            $user = Customer::where('cust_email', $username)->where('cust_del_status', '0')->first();

            $tokenResponse = parent::issueToken($request);

            $content = $tokenResponse->getContent();

            $data = json_decode($content, true);

            $user = collect($user);

            $user->put('access_token', $data['access_token']);

            return response()->json(array($user));
        } catch (ModelNotFoundException $e) {

            return response(["message" => "User not found"], 500);
        } catch (OAuthServerException $e) {

            return response(["message" => "The user credentials were incorrect.', 6, 'invalid_credentials"], 500);
        } catch (\Exception $e) {

            return response(["message" => $e->getMessage()], 500);
        }
    }

}
