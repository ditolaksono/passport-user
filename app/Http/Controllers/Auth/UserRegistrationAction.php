<?php

namespace App\Http\Controllers\Auth;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserRegistrationAction extends Controller
{
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cust_name' => 'required',
            'cust_email' => 'required|unique:customer',
            'cust_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()]);
        }

        $user = Customer::create([
            'cust_email' => $request->get('email'),
            'cust_password' => sha1($request->get('password')),
            'cust_name' => $request->get('name')
        ]);

        $data['token'] = $user->createToken('RoomMeWeb')->accessToken;
        $data['name'] = $user->cust_name;
        $data['message'] = 'Created!';

        return response()->json($data);
    }
}
