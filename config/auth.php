<?php
return [
    'defaults'  => [
        'guard'     => 'api',
        'passwords' => 'customers',
    ],
    'guards'    => [
        'api' => [
            'driver'   => 'passport',
            'provider' => 'customers',
        ],
    ],
    'providers' => [
        'customers' => [
            'driver' => 'eloquent',
            'model'  => \App\Models\Customer::class
        ]
    ]
];
