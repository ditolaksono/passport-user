<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register', 'Auth\UserRegistrationAction');

$router->get('/profile', 'Auth\GetUserProfileAction');

$router->group([
    'prefix' => 'booking'
], function ($router) {
    $router->get('/', 'Booking\ListActiveBookingAction');
});

$router->post('oauth/token', 'Auth\AccessTokenController@issueToken');
